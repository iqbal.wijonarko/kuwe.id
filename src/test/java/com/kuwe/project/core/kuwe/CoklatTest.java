package com.kuwe.project.core.kuwe;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.kuwe.project.createkuwe.core.kuwe.Coklat;
import com.kuwe.project.createkuwe.core.kuwe.Kuwe;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class CoklatTest {

    private Kuwe kuwe;

    @BeforeEach
    public void setUp() {
        kuwe = new Coklat();
    }

    @Test
    public void testMethodGetKuweName() {
        assertEquals("Kuwe Coklat", kuwe.getNamaKuwe());
    }

    @Test
    public void testMethodGetKuweDescription() {
        assertEquals("Kuwe coklat enak bat mantap!!", kuwe.getKuweDescription());
    }

    @Test
    public void testMethodGetKuweTopping() {
        assertTrue(kuwe.getToppings().isEmpty());
    }

}
