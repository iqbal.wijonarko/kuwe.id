package com.kuwe.project.core.toppings;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.kuwe.project.createkuwe.core.kuwe.Vanilla;
import com.kuwe.project.createkuwe.core.toppings.Strawberry;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StrawberryTest {
    private Strawberry strawberry;

    @BeforeEach
    public void setUp() {
        strawberry = new Strawberry(new Vanilla());
    }

    @Test
    public void testMethodGetKuweName() {
        String name = strawberry.getNamaKuwe();
        assertEquals("Kuwe Vanilla", name);
    }

    @Test
    public void testGetMethodKuweDescription() {
        String desc = strawberry.getKuweDescription();
        assertEquals("Kuwe vanilla maknyus", desc);
    }

    @Test
    public void testMethodGetKuweToppings() {
        ArrayList<String> toppings = strawberry.getToppings();
        assertTrue(toppings.contains("Strawberry"));
    }
}
