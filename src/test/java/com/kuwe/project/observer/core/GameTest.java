package com.kuwe.project.observer.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class GameTest {
    private Game game;

    @BeforeEach
    public void setUp() {
        game = new Game();
    }


    @Test
    public void testGameNotDone() {
        assertEquals("Done", game.getGameStatus());
    }

    @Test
    public void testGameDone() {
        game.setGameStatus();
        assertEquals("Done", game.getGameStatus());
    }

    @Test
    public void testIngredientListDifferentWhenAdded() {
        game.addIngredient("Wortel");
        int previousIngredients = game.getIngredientsList().size();
        game.addIngredient("Krim");
        assertNotEquals(previousIngredients, game.getIngredientsList().size());
    }

    @Test
    public void testPhoto() {
        game.setCakePhoto("foto kue stroberi matcha");
        assertEquals(game.getCakePhoto(), "foto kue stroberi matcha");
    }
}
