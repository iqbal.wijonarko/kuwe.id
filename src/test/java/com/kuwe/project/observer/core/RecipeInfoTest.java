package com.kuwe.project.observer.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class RecipeInfoTest {

    private RecipeInfo infoRecipe;
    private Game kue;

    @BeforeEach
    public void setUp() {
        kue = new Game();
        infoRecipe = kue.getRecipeInfo();
    }

    @Test
    public void usesSpoonacularapi() {
        assertEquals("Spoonacular", infoRecipe.getApi());
    }

    @Test
    public void recipeUpdateAfterBroadcast() {
        kue.addIngredient("Stroberi");
        kue.addIngredient("Kelapa");
        assertEquals(kue.getIngredientsList().size(), infoRecipe.getIngredients().size());
    }
}