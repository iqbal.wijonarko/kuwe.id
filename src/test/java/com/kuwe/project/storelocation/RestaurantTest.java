package com.kuwe.project.storelocation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.kuwe.project.storelocation.core.Location;
import com.kuwe.project.storelocation.core.Restaurant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class RestaurantTest {
    private Restaurant restaurant;

    @BeforeEach
    public void setUp() {
        restaurant = new Restaurant();
    }

    @Test
    public void testSetId() {
        long id = 123;
        restaurant.setId(id);

        assertEquals(id, restaurant.getId());
    }

    @Test
    public void testSetName() {
        String name = "Toko kue";
        restaurant.setName(name);

        assertEquals(name, restaurant.getName());
    }

    @Test
    public void testSetUrl() {
        String url = "this is restaurant url";
        restaurant.setUrl(url);

        assertEquals(url, restaurant.getUrl());
    }
}
