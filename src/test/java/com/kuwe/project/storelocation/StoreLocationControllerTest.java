package com.kuwe.project.storelocation;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kuwe.project.storelocation.controller.StoreLocationController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;


@WebMvcTest(controllers = StoreLocationController.class)
public class StoreLocationControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenFindStoreUrlIsAccessed() throws Exception {
        mockMvc.perform(get("/find-form"))
                .andExpect(status().isOk());
    }

    @Test
    public void whenStoreListUrlIsAccessed() throws Exception {
        mockMvc.perform(post("/find-form"))
                .andExpect(status().isOk());
    }


}
