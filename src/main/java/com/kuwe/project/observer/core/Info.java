package com.kuwe.project.observer.core;

import java.util.ArrayList;
import java.util.List;

public abstract class Info {
    protected String api;
    protected Game game;
    public List<String> ingredients = new ArrayList<>();

    public abstract void update();

    public String getApi() {
        return this.api;
    }

    public List<String> getIngredients() {
        return this.ingredients;
    }
}
