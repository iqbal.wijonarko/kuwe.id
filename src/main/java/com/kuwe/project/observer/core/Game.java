package com.kuwe.project.observer.core;

import com.kuwe.project.observer.controller.ObserverController;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("checkstyle:RightCurly")
public class Game {
    private RecipeInfo recipeInfo;
    private NutritionInfo nutritionInfo;
    private List<String> ingredientsList = new ArrayList<>();
    private String gameStatus;
    private String cakePhoto;

    /**
     * Initialize Game. Due to the actual game hasn't been finished yet,
     * Inserts random Strings of ingredients to be displayed on page
     */
    public Game() {
        this.gameStatus = "not Done";
        recipeInfo = new RecipeInfo(this);
        nutritionInfo = new NutritionInfo(this);
        setGameStatus();
    }

    public String getGameStatus() {
        return this.gameStatus;
    }

    public void setGameStatus() {
        this.gameStatus = "Done";
    }

    public void addIngredient(String ingredient) {
        getIngredientsList().add(ingredient);
        broadcast();
    }

    /**
     * get strings of ingredients.
     */
    public String getIngredients() {
        List<String> ingredientsList = getIngredientsList();
        String merged = String.join("_", ingredientsList);
        return merged;
    }

    public List<String> getIngredientsList() {
        return this.ingredientsList;
    }

    private void broadcast() {
        recipeInfo.update();
        nutritionInfo.update();
    }

    public void setCakePhoto(String cakePhoto) {
        this.cakePhoto = cakePhoto;
    }

    public String getCakePhoto() {
        return this.cakePhoto;
    }

    public RecipeInfo getRecipeInfo() {
        return this.recipeInfo;
    }

    public NutritionInfo getNutritionInfo() {
        return this.nutritionInfo;
    }
}