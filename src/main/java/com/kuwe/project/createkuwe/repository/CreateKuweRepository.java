package com.kuwe.project.createkuwe.repository;

import com.kuwe.project.createkuwe.core.kuwe.Kuwe;
import com.kuwe.project.createkuwe.core.toppings.Topping;
import java.util.ArrayList;
import org.springframework.stereotype.Repository;

@Repository
public class CreateKuweRepository {

    /**
     *  repository.
     */
    public void addToppingToKuwe(ArrayList<Kuwe> kuwes, String topping, String kuweName) {

        for (Kuwe kuwe : kuwes) {
            if (kuwe.getNamaKuwe().equals(kuweName)) {

                Kuwe kuweTemp = new Kuwe();

                if (topping.equals("Chocolate")) {
                    kuweTemp = Topping.CHOC_TOPPING.addTopping(kuweTemp);
                } else if (topping.equals("Strawberry")) {
                    kuweTemp = Topping.STRW_TOPPING.addTopping(kuweTemp);
                }

                int index = kuwes.indexOf(kuwe);
                kuwes.set(index, kuweTemp);


            }
        }
    }
}
