package com.kuwe.project.createkuwe.core.kuwe;

import java.util.ArrayList;

public class Coklat extends Kuwe {

    /**
     * Naming chocolate.
     */
    public Coklat() {
        this.namaKuwe = "Kuwe Coklat";
        this.kuweDescription = "Kuwe coklat enak bat mantap!!";
        this.toppings = new ArrayList<>();
    }
}
