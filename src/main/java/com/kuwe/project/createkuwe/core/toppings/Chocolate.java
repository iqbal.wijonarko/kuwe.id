package com.kuwe.project.createkuwe.core.toppings;

import com.kuwe.project.createkuwe.core.kuwe.Kuwe;
import java.util.ArrayList;

public class Chocolate extends Kuwe {

    Kuwe kuwe;

    public Chocolate(Kuwe kuwe) {
        this.kuwe = kuwe;
    }

    @Override
    public String getNamaKuwe() {
        return kuwe.getNamaKuwe();
    }

    @Override
    public String getKuweDescription() {
        return kuwe.getKuweDescription();
    }

    @Override
    public ArrayList<String> getToppings() {
        String topping = "Chocolate";
        kuwe.getToppings().add(topping);

        return kuwe.getToppings();
    }
}
