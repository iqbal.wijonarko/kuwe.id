package com.kuwe.project.createkuwe.core.kuwe;

public enum KuweProducer {
    KUWE_COKLAT,
    KUWE_VANILLA;

    /**
     * create.
     */
    public Kuwe createKuwe() {
        Kuwe kuwe = null;

        if (this == KuweProducer.KUWE_COKLAT) {
            kuwe = new Coklat();
        } else if (this == KuweProducer.KUWE_VANILLA) {
            kuwe = new Vanilla();
        } else {
            kuwe = new Coklat();
        }

        return kuwe;
    }
}
