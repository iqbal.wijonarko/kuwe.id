package com.kuwe.project.createkuwe.service;

import com.kuwe.project.createkuwe.core.kuwe.Kuwe;

public interface CreateKuweService {

    public void addToppings(String topping, String kuwe);

    public Iterable<Kuwe> getAllKuwe();
}
