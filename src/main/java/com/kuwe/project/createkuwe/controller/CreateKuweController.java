package com.kuwe.project.createkuwe.controller;

import com.kuwe.project.createkuwe.service.CreateKuweService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class CreateKuweController {
    //
    //    @Autowired
    //    private CreateKuweService createKuweService;

    /**
     * create.
     */
    @SuppressWarnings("checkstyle:VariableDeclarationUsageDistance")
    @RequestMapping(method = RequestMethod.GET, value = "/create")
    public String create(Model model) {
        ArrayList<String> listKuwe = new ArrayList<>();
        listKuwe.add("Coklat");
        listKuwe.add("Vanilla");
        listKuwe.add("Macha");
        ArrayList<String> listTopping = new ArrayList<>();
        listTopping.add("Vanilla");
        listTopping.add("Strawberry");
        listTopping.add("Coklat");
        listTopping.add("Keju");
        model.addAttribute("kuwes", listKuwe);
        model.addAttribute("toppings", listTopping);
        return "createKuwe";
    }


}
